webpackJsonp([44],{

/***/ 633:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GeofencePageModule", function() { return GeofencePageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__geofence__ = __webpack_require__(729);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ngx_translate_core__ = __webpack_require__(20);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var GeofencePageModule = /** @class */ (function () {
    function GeofencePageModule() {
    }
    GeofencePageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__geofence__["a" /* GeofencePage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["IonicPageModule"].forChild(__WEBPACK_IMPORTED_MODULE_2__geofence__["a" /* GeofencePage */]),
                __WEBPACK_IMPORTED_MODULE_3__ngx_translate_core__["b" /* TranslateModule */].forChild()
            ],
        })
    ], GeofencePageModule);
    return GeofencePageModule;
}());

//# sourceMappingURL=geofence.module.js.map

/***/ }),

/***/ 729:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GeofencePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_google_maps__ = __webpack_require__(76);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var GeofencePage = /** @class */ (function () {
    function GeofencePage(navCtrl, navParams, apiCall, toastCtrl, alerCtrl, modalCtrl, events) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.apiCall = apiCall;
        this.toastCtrl = toastCtrl;
        this.alerCtrl = alerCtrl;
        this.modalCtrl = modalCtrl;
        this.events = events;
        this.devices = [];
        this.creation_type = "circular";
        this.allData = {};
        this.allData._poiListData = [];
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        this.setsmsforotp = localStorage.getItem('setsms');
        this.isdevice = localStorage.getItem('cordinates');
        this.events.subscribe('reloadDetails', function () { });
    }
    GeofencePage.prototype.ngOnInit = function () {
        var _this = this;
        if (localStorage.getItem("SCREEN") != null) {
            this.navBar.backButtonClick = function (e) {
                console.log("back button poped");
                if (localStorage.getItem("SCREEN") != null) {
                    if (localStorage.getItem("SCREEN") === 'live') {
                        _this.navCtrl.setRoot('LivePage');
                    }
                    else {
                        if (localStorage.getItem("SCREEN") === 'dashboard') {
                            _this.navCtrl.setRoot('DashboardPage');
                        }
                    }
                }
            };
        }
        if (this.creation_type == 'circular') {
            this.devices = [];
            this.getCircularGeofence();
        }
        // this.getgeofence();
    };
    GeofencePage.prototype.ngOnDestroy = function () {
        if (this.allData.map) {
            this.allData.map.remove();
        }
    };
    GeofencePage.prototype.radioChecked = function (key) {
        if (key == "circular") {
            this.devices = [];
            this.getCircularGeofence();
        }
        else {
            if (key == "polygon") {
                this.allData._poiListData = [];
                if (this.allData.map) {
                    this.allData.map.remove();
                }
                this.getgeofence();
            }
        }
    };
    GeofencePage.prototype.getCircularGeofence = function () {
        var _this = this;
        this.apiCall.getPoisAPI(this.islogin._id)
            .subscribe(function (data) {
            // this.circularlist = data;
            _this.allData._poiListData = [];
            var i = 0, howManyTimes = data.length;
            var that = _this;
            function f() {
                that.allData._poiListData.push({
                    "geonamename": data[i].poi.poiname,
                    "radius": data[i].radius ? data[i].radius : 'N/A',
                    "address": data[i].poi.address ? data[i].poi.address : 'N/A',
                    "_id": data[i]._id
                });
                that.allData._poiListData[that.allData._poiListData.length - 1].mapid = "a" + i;
                that.allData.map = __WEBPACK_IMPORTED_MODULE_3__ionic_native_google_maps__["b" /* GoogleMaps */].create(that.allData._poiListData[that.allData._poiListData.length - 1].mapid, {
                    camera: {
                        target: {
                            lat: data[i].poi.location.coordinates[1],
                            lng: data[i].poi.location.coordinates[0]
                        },
                        zoom: 13
                    }
                });
                that.allData.map.addMarker({
                    position: {
                        lat: data[i].poi.location.coordinates[1],
                        lng: data[i].poi.location.coordinates[0]
                    }
                });
                var ltln = { "lat": data[i].poi.location.coordinates[1], "lng": data[i].poi.location.coordinates[0] };
                that.allData.map.addCircleSync({
                    'center': ltln,
                    'radius': data[i].radius,
                    'strokeColor': '#d80622',
                    'strokeWidth': 2,
                    'fillColor': 'rgb(255, 128, 128, 0.5)'
                });
                // let circle: Circle = that.allData.map.addCircleSync({
                //   'center': ltln,
                //   'radius': data[i].radius,
                //   'strokeColor': '#d80622',
                //   'strokeWidth': 2,
                //   'fillColor': 'rgb(255, 128, 128, 0.5)'
                // });
                i++;
                if (i < howManyTimes) {
                    setTimeout(f, 200);
                }
            }
            f();
        }, function (err) {
            // debugger
            if (JSON.parse(err._body).message === "no poi list found") {
                var toast = _this.toastCtrl.create({
                    message: "Oops..POI list not found.",
                    duration: 1800,
                    position: 'bottom'
                });
                toast.present();
            }
            else {
                return;
            }
            // console.log("error in pois: ", JSON.parse(JSON.stringify(err)).message)
            console.log("error in pois: ", JSON.parse(err._body).message);
        });
    };
    GeofencePage.prototype.addgeofence = function () {
        this.navCtrl.push('AddGeofencePage');
    };
    GeofencePage.prototype.getgeofence = function () {
        var _this = this;
        this.apiCall.startLoading().present();
        this.apiCall.getallgeofenceCall(this.islogin._id)
            .subscribe(function (data) {
            _this.apiCall.stopLoading();
            _this.devices = data;
        }, function (err) {
            _this.apiCall.stopLoading();
            console.log("error => ", err);
        });
    };
    GeofencePage.prototype.deleteGeo = function (_id) {
        var _this = this;
        // this.apiCall.startLoading().present();
        this.apiCall.deleteGeoCall(_id).
            subscribe(function (data) {
            // this.apiCall.stopLoading();
            _this.DeletedDevice = data;
            var toast = _this.toastCtrl.create({
                message: 'Deleted Geofence Area successfully.',
                position: 'bottom',
                duration: 2000
            });
            toast.onDidDismiss(function () {
                console.log('Dismissed toast');
                _this.getgeofence();
            });
            toast.present();
        }, function (err) {
            // this.apiCall.stopLoading();
            var body = err._body;
            var msg = JSON.parse(body);
            var alert = _this.alerCtrl.create({
                title: 'Oops!',
                message: msg.message,
                buttons: ['OK']
            });
            alert.present();
        });
    };
    GeofencePage.prototype.DelateGeofence = function (_id) {
        var _this = this;
        var alert = this.alerCtrl.create({
            message: 'Do you want to delete this geofence area?',
            buttons: [{
                    text: 'No'
                },
                {
                    text: 'YES',
                    handler: function () {
                        _this.deleteGeo(_id);
                    }
                }]
        });
        alert.present();
    };
    GeofencePage.prototype.DisplayDataOnMap = function (item) {
        var _this = this;
        // console.log(item);
        // var baseURLp = 'http://13.126.36.205:3000/geofencing/geofencestatus?gid=' + item._id + '&status=' + item.status + '&entering=' + item.entering + '&exiting=' + item.exiting;
        this.apiCall.geofencestatusCall(item._id, item.status, item.entering, item.exiting)
            .subscribe(function (data) {
            _this.statusofgeofence = data;
            // console.log(this.statusofgeofence);
        }, function (err) {
            console.log(err);
        });
    };
    GeofencePage.prototype.geofenceShow = function (item) {
        this.navCtrl.push('GeofenceShowPage', {
            param: item
        });
    };
    GeofencePage.prototype.deletePOI = function (data) {
        var _this = this;
        console.log("delete data: ", data);
        var alert = this.alerCtrl.create({
            message: "Do you want to delete this Geofence?",
            buttons: [{
                    text: 'BACK'
                }, {
                    text: 'YES PROCEED',
                    handler: function () {
                        _this.apiCall.startLoading().present();
                        _this.apiCall.deletePOIAPI(data._id)
                            .subscribe(function (data) {
                            _this.apiCall.stopLoading();
                            var toast = _this.toastCtrl.create({
                                message: "Geofence deleted successfully!",
                                position: "top",
                                duration: 2000
                            });
                            toast.present();
                            _this.getCircularGeofence();
                        }, function (err) {
                            _this.apiCall.stopLoading();
                            console.log("error occured while deleting POI: ", err);
                            var toast = _this.toastCtrl.create({
                                message: "Geofence deleted successfully!",
                                position: "top",
                                duration: 2000
                            });
                            toast.present();
                            _this.getCircularGeofence();
                        });
                    }
                }]
        });
        alert.present();
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Navbar"]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Navbar"])
    ], GeofencePage.prototype, "navBar", void 0);
    GeofencePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-geofence',template:/*ion-inline-start:"D:\New\zippco-gps\src\pages\geofence\geofence.html"*/'<ion-header>\n\n  <ion-navbar>\n\n    <ion-title>{{ "Geofences" | translate }}</ion-title>\n\n    <ion-buttons end>\n\n      <button ion-button icon-only (tap)="addgeofence()">\n\n        <ion-icon name="add"></ion-icon>\n\n      </button>\n\n    </ion-buttons>\n\n  </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content>\n\n  <div style="padding: 10px;">\n\n    <div\n\n      style="padding: 0px 10px 10px;margin-top: -10px; background: white; border-radius: 5px;"\n\n    >\n\n      <input\n\n        type="radio"\n\n        name="demo"\n\n        [(ngModel)]="creation_type"\n\n        value="circular"\n\n        id="radio-one"\n\n        class="form-radio"\n\n        (click)="radioChecked(\'circular\')"\n\n      /><label for="radio-one">{{ "Circular" | translate }}</label>\n\n      <input\n\n        type="radio"\n\n        name="demo"\n\n        [(ngModel)]="creation_type"\n\n        value="polygon"\n\n        id="radio-one"\n\n        class="form-radio"\n\n        (click)="radioChecked(\'polygon\')"\n\n      /><label for="radio-two">{{ "Polygon" | translate }}</label>\n\n    </div>\n\n  </div>\n\n  <div *ngIf="devices.length > 0">\n\n    <ion-card *ngFor="let item of devices">\n\n      <ion-item (click)="geofenceShow(item)">\n\n        <h2>{{ item.geoname }}</h2>\n\n        <p style="margin-top: 3%;">\n\n          {{ "Number Of Vehicles" | translate }} - {{ item.devicesWithin.length }}\n\n        </p>\n\n        <ion-icon\n\n          item-end\n\n          name="trash"\n\n          color="danger"\n\n          (tap)="DelateGeofence(item._id)"\n\n        >\n\n        </ion-icon>\n\n      </ion-item>\n\n      <!-- <ion-toggle color="danger"></ion-toggle> -->\n\n      <ion-row>\n\n        <ion-col width-33>\n\n          <div style="padding-left: 10px;">\n\n            <ion-toggle\n\n              [(ngModel)]="item.status"\n\n              (ionChange)="DisplayDataOnMap(item)"\n\n              color="danger"\n\n            ></ion-toggle>\n\n          </div>\n\n\n\n          <!-- <ion-toggle [(ngModel)]="item.status" (ionChange)="DisplayDataOnMap(item)" color="danger" style="margin-left:11%;"></ion-toggle> -->\n\n          <p style="color:#e618af;font-size:10px;margin-left: 10px;">\n\n            {{ "Display on map" | translate }}\n\n          </p>\n\n        </ion-col>\n\n        <ion-col width-33 style="text-align: center;">\n\n          <div style="padding-left: 10px;">\n\n            <ion-toggle\n\n              [(ngModel)]="item.entering"\n\n              (ionChange)="DisplayDataOnMap(item)"\n\n              color="danger"\n\n            ></ion-toggle>\n\n          </div>\n\n          <!-- <ion-toggle [(ngModel)]="item.entering" (ionChange)="DisplayDataOnMap(item)" color="danger" style="margin-left: 20%;"></ion-toggle> -->\n\n          <p style="color:green;font-size: 10px;margin-left: -8%;">\n\n            {{ "Notification Entering" | translate }}\n\n          </p>\n\n        </ion-col>\n\n        <ion-col width-33>\n\n          <div style="padding-left: 10px;">\n\n            <ion-toggle\n\n              [(ngModel)]="item.exiting"\n\n              (ionChange)="DisplayDataOnMap(item)"\n\n              color="danger"\n\n            ></ion-toggle>\n\n          </div>\n\n          <!-- <ion-toggle [(ngModel)]="item.exiting" (ionChange)="DisplayDataOnMap(item)" color="danger" style="margin-left: 20%;"></ion-toggle> -->\n\n          <p style="color:green;font-size: 10px; margin-left: 11%;">\n\n            {{ "Notification Exiting" | translate }}\n\n          </p>\n\n        </ion-col>\n\n      </ion-row>\n\n    </ion-card>\n\n  </div>\n\n  <div *ngIf="allData._poiListData.length > 0">\n\n    <ion-card *ngFor="let list of allData._poiListData">\n\n      <ion-item>\n\n        <ion-thumbnail item-start>\n\n          <div id="{{ list.mapid }}" style="height: 100px; width: 100px;"></div>\n\n        </ion-thumbnail>\n\n        <h1>{{ list.geonamename }}</h1>\n\n        <p style="color: #687DCA">\n\n          {{ "Radius:" | translate }} {{ list.radius }}\n\n        </p>\n\n        <p><ion-icon name="pin"></ion-icon>&nbsp;&nbsp;{{ list.address }}</p>\n\n        <p>\n\n          <!-- <button ion-button round small (click)="editPOI(list)">Edit</button> -->\n\n          <button\n\n            ion-button\n\n            round\n\n            small\n\n            style="background: #a60033; color: white"\n\n            (click)="deletePOI(list)"\n\n          >\n\n            {{ "Delete" | translate }}\n\n          </button>\n\n        </p>\n\n      </ion-item>\n\n    </ion-card>\n\n  </div>\n\n</ion-content>\n\n'/*ion-inline-end:"D:\New\zippco-gps\src\pages\geofence\geofence.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["AlertController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ModalController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Events"]])
    ], GeofencePage);
    return GeofencePage;
}());

//# sourceMappingURL=geofence.js.map

/***/ })

});
//# sourceMappingURL=44.js.map