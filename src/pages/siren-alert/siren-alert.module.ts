import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SirenAlertPage } from './siren-alert';

@NgModule({
  declarations: [
    SirenAlertPage,
  ],
  imports: [
    IonicPageModule.forChild(SirenAlertPage),
  ],
})
export class SirenAlertPageModule {}
